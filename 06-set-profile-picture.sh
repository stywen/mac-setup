#!/bin/sh
#           _                         __ _ _            _      
#  ___  ___| |_      _ __  _ __ ___  / _(_) | ___ _ __ (_) ___ 
# / __|/ _ \ __|____| '_ \| '__/ _ \| |_| | |/ _ \ '_ \| |/ __|
# \__ \  __/ ||_____| |_) | | | (_) |  _| | |  __/ |_) | | (__ 
# |___/\___|\__|    | .__/|_|  \___/|_| |_|_|\___| .__/|_|\___|
#                   |_|                          |_|           

if brew ls --versions wget > /dev/null; then
  echo "$(tput setaf 2)wget is installed"
else
  echo "$(tput setaf 1)wget is missing"
  echo "$(tput setaf 3)wget will be installed"
fi

#Path where profile pictures should be stored /Users/stywen/Pictures/ProfilePicture
PATH_PPIC="/Users/stywen/Pictures/ProfilePicture"

#Path+name of the profile picture
PPIC="$PATH_PPIC/ppic1.png"

echo "$(tput setaf 3)DEBUG $PPIC"

#check if the directory exists, if not create it (profile pictures directory)
if [ -f "$PPIC" ]           
then
    echo "$(tput setaf 0)DEBUG File $PPIC exists."          
else
    echo "Error: File $PPIC does not exists."
    echo "$PPIC will be set up"
    mkdir -p $PATH_PPIC
    wget --output-document=$PPIC https://pbs.twimg.com/media/EED6p7pXYAAtvbW?format=jpg&name=900x900 
    # set new user profile picture
    ## delete current profile picture
    sudo dscl . delete /Users/stywen JPEGPhoto
    sudo dscl . delete /Users/stywen Picture

    ## set the predefined profile picture
    sudo dscl . create /Users/stywen Picture $PPIC
fi
