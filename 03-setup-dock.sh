#!/bin/sh

#locate the Dock left
defaults write com.apple.Dock orientation -string left 

#disable recent apps from dock
defaults write com.apple.dock show-recents -bool FALSE

#remove all apps from dock
defaults write com.apple.dock persistent-apps -array

# hide Dock 
osascript -e "tell application \"System Events\" to set the autohide of the dock preferences to true"

# faster dock 
defaults write com.apple.dock autohide-time-modifier -int 0

defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Brave Browser.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"

defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Telegram Desktop.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"

defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Whatsapp.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"

defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/iTerm.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"

killall Dock

