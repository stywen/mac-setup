#!/bin/sh
#           _        _                _                                   _ 
#  ___  ___| |_     | |__   __ _  ___| | ____ _ _ __ ___  _   _ _ __   __| |
# / __|/ _ \ __|____| '_ \ / _` |/ __| |/ / _` | '__/ _ \| | | | '_ \ / _` |
# \__ \  __/ ||_____| |_) | (_| | (__|   < (_| | | | (_) | |_| | | | | (_| |
# |___/\___|\__|    |_.__/ \__,_|\___|_|\_\__, |_|  \___/ \__,_|_| |_|\__,_|
#                                         |___/                             

if brew ls --versions wget > /dev/null; then
  echo "$(tput setaf 2)wget is installed"
else
  echo "$(tput setaf 1)wget is missing"
  echo "$(tput setaf 3)wget will be installed"
fi

#Path where wallpapers should be stored /Users/stywen/Pictures/Wallpapers
PATH_WALLPAPER="/Users/stywen/Pictures/Wallpapers"

#Path+name of the wallpaper
BACKGROUND="$PATH_WALLPAPER/wallpaper1.jpg"

echo "$(tput setaf 3)DEBUG $BACKGROUND"

#check if the directory exists, if not create it (wallpaper directory)
if [ -f "$BACKGROUND" ]
then
    echo "File $BACKGROUND exists."
else
    echo "Error: File $BACKGROUND does not exists."
    echo "$BACKGROUND will be set up"
    mkdir -p $PATH_WALLPAPER
    wget --output-document=$BACKGROUND https://images.hdqwalls.com/wallpapers/abstract-colorful-4k-zx.jpg
fi

# set wallpaper
osascript -e 'tell application "System Events" to tell every desktop to set picture to "/Users/stywen/Pictures/Wallpapers/wallpaper1.jpg"'
