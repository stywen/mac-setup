#!/bin/sh
#  _                                             _                                         _       _   
# | |__  _ __ _____      __     _ __   __ _  ___| | ____ _  __ _  ___       ___   ___ _ __(_)_ __ | |_ 
# | '_ \| '__/ _ \ \ /\ / /____| '_ \ / _` |/ __| |/ / _` |/ _` |/ _ \_____/ __| / __| '__| | '_ \| __|
# | |_) | | |  __/\ V  V /_____| |_) | (_| | (__|   < (_| | (_| |  __/_____\__ \| (__| |  | | |_) | |_ 
# |_.__/|_|  \___| \_/\_/      | .__/ \__,_|\___|_|\_\__,_|\__, |\___|     |___/ \___|_|  |_| .__/ \__|
#                              |_|                         |___/                            |_|             

homebrew_packages=("git" "wget" "curl" "neofetch")
for str in ${homebrew_packages[@]}; do
  brew install $str
done

homebrew_cask=("brave-browser" "telegram-desktop" "whatsapp" "iterm2")
for str in ${homebrew_cask[@]}; do
  brew install --cask $str
done

brew install --cask affinity-photo
brew install --cask sublime-text
brew install --cask vscodium
brew install --cask all-in-one-messenger
brew install --cask remote-desktop-manager-free
brew install --cask alacritty
