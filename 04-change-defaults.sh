#!/bin/sh

#expand save panel by default
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

#display file extensions
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# crash reports as notification
defaults write com.apple.CrashReporter UseUNC 1

# finder set default view to list
defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"
defaults write com.apple.Finder FXPreferredViewStyle Nlsv

cp ./files/com.apple.finder.plist ~/Library/Preferences/com.apple.finder.plist
killall Dock
